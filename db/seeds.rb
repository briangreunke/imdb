# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Movie.destroy_all
Actor.destroy_all
ActorMovie.destroy_all
Review.destroy_all

10.times do
  Movie.create!(
    title: Faker::Book.title,
    rating: rand(1..5)
  )
end

10.times do
  Actor.create!(
    first_name: Faker::Name.first_name,
    last_name: Faker::Name.last_name
  )
end

5.times do
  ActorMovie.create!(
    actor: Actor.order('RANDOM()').first,
    movie: Movie.order('RANDOM()').first
  )
end

20.times do
  User.create!(
    email: Faker::Internet.email,
    password: Faker::Internet.password
  )
end

43.times do
  Review.create!(
    movie: Movie.order('Random()').first,
    comment: Faker::Lorem.sentence,
    rating: 3,
    reviewed_by: User.order('RANDOM()').first.id
  )
end
