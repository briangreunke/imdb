class AddReviewedByColumn < ActiveRecord::Migration[6.0]
  def change
    add_column :reviews, :reviewed_by, :string
  end
end
