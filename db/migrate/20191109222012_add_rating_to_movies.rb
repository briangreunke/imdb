class AddRatingToMovies < ActiveRecord::Migration[6.0]
  def change
    add_column :movies, :rating, :decimal
  end
end
