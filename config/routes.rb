Rails.application.routes.draw do
  
  resources :actors do
    #resources :actor_movies
  end

  resources :movies do
    resources :reviews
    #resources :actor_movies
  end

  get 'actors/:id/movies', to: 'actors#movies'
  get 'movies/:id/actors', to: 'movies#actors'

  get 'movies/rating/:rating', to: 'movies#rating'

  post 'auth/login', to: 'authentication#authenticate'
  post 'signup', to: 'users#create'
end
