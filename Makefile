serve: ## Serve the current dev env
	@rails s -b 0.0.0.0

seed:  ## Seed the database
	@rails db:migrate RAILS_ENV=development
	@rails db:seed

test: ## Run unit tests
	@bundle exec rspec

.PHONY: help

help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

.DEFAULT_GOAL := help

