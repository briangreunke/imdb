# IMDB example project

This is a Ruby API which mimics IMBD.

The API allows for:
- User authentication via a JWT
- Querying actors by movie
- Querying movies by actor
- Users to rate movies
- Querying movies by rating
- Associating reviews to a user

## Building

```
# Install the requirements
bundle install

# Seed the database
make seed

# Serve on local port 3000
make serve
```

### Use the Dockerfile

```
docker build -t imdb .
docker run -itd --name imdb -v ${PWD}:/imdb -p 3000:3000 --rm imdb bash -c "make seed && make serve"
```

## Sources
The following resources were used/referenced to build this.

- [https://scotch.io/tutorials/build-a-restful-json-api-with-rails-5-part-one](https://scotch.io/tutorials/build-a-restful-json-api-with-rails-5-part-one)
- [https://guides.rubyonrails.org/](https://guides.rubyonrails.org/)
