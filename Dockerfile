FROM ruby:2.6.5-buster

RUN gem install rails

RUN apt-get update -y && apt-get install sqlite3 -y

#RUN groupadd -g 1000 rails

#RUN useradd -m -s /bin/bash -u 1000 -g 1000 rails

#RUN usermod -aG sudo rails

#RUN chown -R 1000:1000 /usr/local/bundle

#RUN apt-get install vim -y

#USER 1000:1000

COPY $PWD /imdb

#RUN chown -R 1000:1000 /imdb

WORKDIR /imdb

RUN bundle install


