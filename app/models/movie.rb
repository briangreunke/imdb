class Movie < ApplicationRecord

  # The following two lines define the many-to-many
  # relationship between actor/movie.
  # I.e. an actor can be in many movies and a movie can
  # have many actors
  has_many :actor_movies, dependent: :destroy
  has_many :actors, through: :actor_movies

  # This defines a one-to-many relationship
  # I.e. a movie can have many reviews
  has_many :reviews

  validates_presence_of :title

  validates :rating, numericality: true
end
