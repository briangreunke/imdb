class Actor < ApplicationRecord

  # The following two lines build a many-to-many relationship.
  # I.e. a movie can have >= 1 actor, and an actor can be in
  # >= 1 movie.
  has_many :actor_movies, dependent: :destroy
  has_many :movies, through: :actor_movies

  validates_presence_of :first_name, :last_name
end
