class Review < ApplicationRecord

  # This defines a many-to-one relationship
  # I.e. a review is associated to a movie and
  # a movie can have many reviews
  belongs_to :movie

  validates :comment, presence: true, length: { minimum: 1, maximum: 10000 }

  # TODO: Should this be a polymorphic relationship?
  validates :reviewed_by, presence: true

  # This validates that rating is a number between: 1-5
  validates :rating, numericality: { only_integer: true, greater_than_or_equal_to: 1, less_than_or_equal_to: 5 }, presence: true

end
