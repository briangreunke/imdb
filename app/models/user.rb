class User < ApplicationRecord
  has_secure_password

  # This defines the ownership relationship to reviews
  has_many :reviews, as: :reviewable, foreign_key: :reviewed_by

  validates :email, presence: true
  validates :password_digest, presence: true

end
