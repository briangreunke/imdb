class ActorMovie < ApplicationRecord
  # The following two lines define a foreign key relationship
  # between this model and the Actor/Movie models.
  # Using this construct allows for the many-to-many relationship
  belongs_to :actor
  belongs_to :movie
end
