class MoviesController < ApplicationController
    before_action :set_movie, only: [:show, :actors]
    skip_before_action :authorize_request

    def index
      @movies = Movie.all

#      for movie in @movies
#        if movie.rating.nil?
#          unless movie.reviews.nil?
#            @movie.update( rating: @movie.reviews.average(:rating) )
#          end
#        end
#      end
      json_response(@movies)
    end

    def show
      json_response(@movie)
    end

    def actors
      json_response(@movie.actors)
    end

    def rating
      #json_response(Movie.all)
      json_response(@movies = Movie.where("rating > ?", params[:rating].to_d))
    end

    private

    def movie_params
      params.permit(:title)
    end

    def set_movie
      @movie = Movie.find( params[:id] )
    end

    def calc_rating
      @movie.reviews.average(:rating)
    end
end
