class ReviewsController < ApplicationController
  before_action :set_movie
  before_action :set_review, only: [ :show ]
  skip_before_action :authorize_request, only: [ :index, :show ]

  def index
    json_response(@movie.reviews)
  end

  def show
    json_response(@review)
  end

  def create

    # First, create the review, using the current user as the "reviewed by"
    @review = @movie.reviews.create!(rating: review_params[ :rating ], comment: review_params[ :comment ], reviewed_by: current_user.id )
 
    # When a new review is added, recalculate the movie rating
    @movie.update( rating: calc_rating ) # Update the movie rating when a new review is created
    
    json_response(@review, :created)
  end


  private

  def review_params
    params.permit( :rating, :comment )
  end

  def set_movie
    @movie = Movie.find( params[ :movie_id ] )
  end

  def set_review
    @review = @movie.reviews.find_by!( id: params[:id]) if @movie
  end

  def calc_rating
    @movie.reviews.average( :rating )
  end
end
