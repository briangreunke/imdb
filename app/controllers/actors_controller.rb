class ActorsController < ApplicationController

  before_action :set_actor, only: [:show, :movies]
  skip_before_action :authorize_request

  def index
    @actors = Actor.all
    json_response(@actors)
  end

  def show
    json_response(@actor)
  end

  def movies
    json_response(@actor.movies)
  end

  private

  def actor_params
    params.permit(:first_name, :last_name)
  end

  def set_actor
    @actor = Actor.find( params[:id] )
  end
end
