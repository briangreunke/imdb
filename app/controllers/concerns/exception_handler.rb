module ExceptionHandler

  extend ActiveSupport::Concern


  class AuthenticationError < StandardError; end
  class MissingToken < StandardError; end
  class InvalidToken < StandardError; end

#  included do
#    rescue_from ActiveRecord::RecordNotFound do |e|
#      json_response({ message: e.message }, :not_found )
#    end

#    rescue_from ActiveRecord::RecordInvalid do |e|
#      json_response({ message: e.message}, :unprocessable_entity )
#    end

  included do
    rescue_from ActiveRecord::RecordNotFound, with: :not_found
    rescue_from ActiveRecord::RecordInvalid, with: :unprocessible
    rescue_from ExceptionHandler::AuthenticationError, with: :unauthorized
    rescue_from ExceptionHandler::MissingToken, with: :unprocessible
    rescue_from ExceptionHandler::InvalidToken, with: :unprocessible
  end

  private

  def not_found(e)
    json_response({ messsage: e.message }, :not_found )
  end

  def unprocessible(e)
    json_response({ message: e.message}, :unprocessable_entity )
  end

  def unauthorized(e)
    json_response({ message: e.message }, :unauthorized)
  end
end
