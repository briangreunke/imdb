class ApplicationController < ActionController::API
  include Response
  include ExceptionHandler

  # This causes authentication to happen on all requests
  # Check specific controllers for overrides
  # E.g. signup and login
  before_action :authorize_request

  attr_reader :current_user

  private

  def authorize_request
    @current_user = (AuthorizeApiRequest.new(request.headers).call)[:user]
  end

end
