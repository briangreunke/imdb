class UsersController < ApplicationController

  # Stops the automatic authorization action built in the 
  # ApplicationController from occurring and preventing new
  # users from signing up
  skip_before_action :authorize_request, only: [:create]

  def create
    user = User.create!(user_params)
    auth_token = AuthenticateUser.new(user.email, user.password).call
    response = { message: Message.account_created, auth_token: auth_token }
    json_response( response, :created )
  end

  private

  def user_params
    params.permit( :email, :password, :password_confirmation )
  end
end
