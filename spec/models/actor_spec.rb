require 'rails_helper'

RSpec.describe Actor, type: :model do

  it { should have_many(:actor_movies).dependent(:destroy) }
  it { should have_many(:movies) }

  it { should validate_presence_of(:first_name) }
  it { should validate_presence_of(:last_name) }

end
