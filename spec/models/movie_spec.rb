require 'rails_helper'

RSpec.describe Movie, type: :model do

  it { should have_many(:actor_movies).dependent(:destroy) }
  it { should have_many(:actors) }

  it { should have_many(:reviews) }

  it { should validate_presence_of(:title) }

  it { should validate_numericality_of(:rating) }

end
