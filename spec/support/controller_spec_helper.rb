module ControllerSpecHelper

  # Generate tokens from a mocked user

  def token_generator(user_id)
    JsonWebToken.encode(user_id: user_id)
  end

  def expired_token_generator(user_id)
    JsonWebToken.encode({ user_id: user_id}, ( Time.now.to_i - 8 ))
  end


  # Generate headers (using the token)
  #
  def valid_headers
    {
      "Authorization" => token_generator(user.id),
      "Content-type" => "application/json"
    }
  end

  def invalid_headers
    {
      "Authorization" => nil,
      "Content-type" => "application/json"
    }
  end

  # Build credential pairs
  def build_credentials(email, password)
    {
      email: email,
      password: password
    }.to_json
  end
end
