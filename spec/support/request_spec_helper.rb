module RequestSpecHelper

  # Make into json
  def json
    JSON.parse(response.body)
  end

  # Used for review requests
  VALID_COMMENT = 'A generic comment'
  VALID_RATING = 4

  LOW_RATING = 0
  HIGH_RATING = 6

  VALID_USER = 2

  # > 10000 characters
  LONG_COMMENT = "This comment is too long." * 410
  # < 1 character
  SHORT_COMMENT = ""

  def valid_review_attributes
    build_review_attributes()
  end

  def review_low_rating
    build_review_attributes(rating=LOW_RATING)
  end

  def review_high_rating
    build_review_attributes(rating=HIGH_RATING)
  end

  def review_no_rating
    build_review_attributes(rating=nil)
  end

  def review_no_comment
    build_review_attributes(comment=nil)
  end

  def review_short_comment
    build_review_attributes(comment=SHORT_COMMENT)
  end

  def review_long_comment
    build_review_attributes(comment=LONG_COMMENT)
  end

  def review_no_attributes
    {}.to_json  # return empty json
  end
  
  private

  def build_review_attributes( rating=VALID_RATING, comment=VALID_COMMENT, reviewed_by=VALID_USER )
    {
      "rating": rating,
      "comment": comment,
      "reviewed_by": reviewed_by
    }.to_json
  end
end
