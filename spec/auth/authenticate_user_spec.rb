require 'rails_helper'

RSpec.describe AuthenticateUser do

  let( :user ) { create( :user ) }

  subject( :valid_auth_obj ) { described_class.new( user.email, user.password ) }

  describe '#call' do
    context 'when the credentials are valid' do
      it 'returns an auth token' do
        token = valid_auth_obj.call
        expect( token ).not_to be_nil
      end
    end

    context 'when the credentials are invalid' do
      context 'when the credentials are missing' do
        subject( :invalid_auth_obj ) { described_class.new() }

        it 'raises an authentication error' do
          expect{ invalid_auth_obj.call }.to raise_error(
            ExceptionHandler::AuthenticationError, /Invalid credentials/
          )
        end
      end

      context 'when the credentials are invalid' do
        subject( :invalid_auth_obj ) { described_class.new( "fakeuser", "fakepassword" ) }

        it 'raises an authentication error' do
          expect{ invalid_auth_obj.call }.to raise_error(
            ExceptionHandler::AuthenticationError, /Invalid credentials/
          )
        end
      end
    end
  end
end
