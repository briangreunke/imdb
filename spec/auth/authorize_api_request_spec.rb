require 'rails_helper'

RSpec.describe AuthorizeApiRequest do

  let(:user) { create(:user) }

  let(:header) {{ 'Authorization' => token_generator(user.id) }}

  subject(:invalid_request_obj) { described_class.new({}) }

  subject(:request_obj) { described_class.new( header ) }

  describe '#call' do
    context 'when request is valid' do
      it 'returns a user object' do
        result = request_obj.call
        expect(result[:user]).to eq(user)
      end
    end

    context 'when the request is invalid' do
      context 'when the token is missing' do
        it 'returns a MissingToken error' do
          expect { invalid_request_obj.call }.to raise_error(
            ExceptionHandler::MissingToken, 'Missing token')
        end
      end

      context 'when the token is invalid' do

        # Create an invalid token
        subject(:invalid_request_obj) do
          described_class.new('Authorization' => token_generator(5))
        end

        it 'returns an InvalidToken error' do
          expect { invalid_request_obj.call }.to raise_error(
            ExceptionHandler::InvalidToken, /Invalid token/)
        end
      end

      context 'when the token has expired' do
        let(:header) {{ 'Authorization' => expired_token_generator( user.id ) }}
        subject(:request_obj) { described_class.new( header ) }

        it 'raises an ExpiredToken error' do
          expect { request_obj.call }.to raise_error(
            ExceptionHandler::InvalidToken, /Signature has expired/ )  # This message is automatically generated, not defined by user.
        end
      end

      context 'when the token is spoofed' do
        let(:header) {{ 'Authorization' => 'fzJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxLCJleHAiOjE1NzMyNTU2MTR9.jT47KmIddb6FMJNU22ZLUIgfKrs-Qq076Z7CB9QanqZ' }}
        subject(:request_obj) { described_class.new( header ) }

        it 'raises an InvalidToken error' do
          expect { request_obj.call }.to raise_error(
            ExceptionHandler::InvalidToken
          )
        end
      end
    end
  end
end
