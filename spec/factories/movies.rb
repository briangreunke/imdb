FactoryBot.define do
  factory :movie do
    title { Faker::Restaurant.name }
    rating { rand(1..5) }
  end
end
