FactoryBot.define do
  factory :review do
    movie
    rating { 3 }
    comment { Faker::Lorem.sentence }
    reviewed_by { rand(1..10) }
  end
end
