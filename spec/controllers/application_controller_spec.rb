require "rails_helper"

RSpec.describe ApplicationController, type: :controller do

  let!( :user ) { create( :user ) } 
  let( :headers ) { valid_headers }
  let( :invalid_headers ) { valid_headers.except( 'Authorization' ) }

  describe "#authorize_request" do
    context "when auth token is passed" do
      
      # This is needed for a `controller` vs `request` test
      before { allow( request ).to receive( :headers ).and_return( headers ) }

      it "sets the current user" do
        # Execute the private `authorize_request` function
        # which should return the current user
        expect( subject.instance_eval { authorize_request }).to eq( user )
      end
    end

    context "when auth token is not passed" do
      before { allow( request ).to receive( :headers ).and_return( invalid_headers ) }

      it "raises MissingToken error" do
        expect { subject.instance_eval { authorize_request } }.
          to raise_error( ExceptionHandler::MissingToken, /Missing token/ )
      end
    end
  end
end
