## TODO:
# 1. Build constant messages

require 'rails_helper'

RSpec.describe 'Reviews API', type: :request do

  let!(:user) { create(:user) }
  let!(:movie) { create( :movie ) }
  let(:movie_id) { movie.id }
  let!(:reviews) { create_list( :review, 3, movie_id: movie_id, reviewed_by: user.id, rating: 3 )}
  let(:review_id) { reviews.first.id }
  let(:headers) { valid_headers }


  describe 'GET /movies/:movie_id/reviews' do

    context 'when the movie exists' do
      before { get "/movies/#{movie_id}/reviews" }
   
      it 'returns the movie reviews' do
        expect(json).not_to be_empty
        expect(json.size).to eq(3)
      end
  
      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end

      it 'is related to the user' do
        # Returns a list of all reviews, grab the first one
        expect(json[0]["reviewed_by"].to_i).to eq(user.id)
      end
    end
 
    context 'when the movie/id does not exist' do
      before { get "/movies/0/reviews" } # Movie 0 does not exist

      it 'return status code 404' do
        expect(response).to have_http_status(404)
      end

      it 'returns a \'Not found\' message' do
        expect(response.body).to match(/Couldn't find Movie/)
      end
    end
  end

end
