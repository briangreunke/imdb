# Note: attributes (params) are pulled from the support module

require 'rails_helper'

RSpec.describe 'Reviews API', type: :request do

  # Create a user, a movie, and then create reviews
  # from that user, for a movie
  # A bit of magic happens w/ `headers` and `user`
  # where the `headers` need the `user` for the `user.id` to 
  # create a token
  let!(:user) { create(:user) }
  let!(:movie) { create( :movie ) }
  let(:movie_id) { movie.id }
  let!(:reviews) { create_list( :review, 3, movie_id: movie_id, rating: 3 )}
  let(:headers) { valid_headers }


  describe 'POST /movies/:movie_id/reviews' do

    context 'when attributes are invalid' do

      context 'when rating attribute is too low' do

        before { post "/movies/#{movie_id}/reviews", params: review_low_rating, headers: headers }
  
        it 'returns status code 422' do
          expect(response).to have_http_status(422)
        end
      end
  
      context 'when rating attribute is too high' do

        before { post "/movies/#{movie_id}/reviews", params: review_high_rating, headers: headers }
  
        it 'returns status code 422' do
          expect(response).to have_http_status(422)
        end
      end
  
      context 'when rating is missing' do

        before { post "/movies/#{movie_id}/reviews", params: review_no_rating, headers: headers }
  
        it 'returns status code 422' do
          expect(response).to have_http_status(422)
        end
      end
  
      context 'when comment attribute is missing' do

        before { post "/movies/#{movie_id}/reviews", params: review_no_comment, headers: headers }
  
        it 'returns status code 422' do
          expect(response).to have_http_status(422)
        end
      end
  
      context 'when comment length is empty' do

        before { post "/movies/#{movie_id}/reviews", params: review_short_comment, headers: headers }
  
        it 'returns status code 422' do
          expect(response).to have_http_status(422)
        end
      end
  
      context 'when comment length is too long' do

        before { post "/movies/#{movie_id}/reviews", params: review_long_comment, headers: headers }
  
        it 'returns status code 422' do
          expect(response).to have_http_status(422)
        end
      end
  
      context 'when attributes are empty' do

        before { post "/movies/#{movie_id}/reviews", params: review_no_attributes, headers: headers }
  
        it 'returns status code 422' do
          expect(response).to have_http_status(422)
        end
      end
    end
  end
end
