## TODO:
# 1. Build constant messages

require 'rails_helper'

RSpec.describe 'Reviews API', type: :request do

  # Create a user, who creates reviews on a movie
  # The `headers` helper uses the `user` to create a token
  # for authentication and authorization
  let!(:user) { create(:user) }
  let!(:movie) { create( :movie ) }
  let(:movie_id) { movie.id }
  let!(:reviews) { create_list( :review, 3, movie_id: movie_id, rating: 3 )}
  let(:review_id) { reviews.first.id }
  let(:headers) { valid_headers }


  describe 'POST /movies/:movie_id/reviews' do

    context 'when review attributes are valid' do
  
      before do
        post "/movies/#{movie_id}/reviews", params: valid_review_attributes, headers: headers

        # Creating a new review has the side effect of updating the movie.
        # Therefore, we need to reload the movie here
        movie.reload
      end

      it 'returns status code 201' do
        expect(response).to have_http_status(201)
      end

      it 'calculates the correct rating' do
        # Create 3 ratings of "3" and one of "4"
        expect(movie.rating).to eq(3.25)
      end

      it 'is \'reviewed_by\' the user' do
        expect( json['reviewed_by'].to_i ).to eq( user.id )
      end

      context 'but the authorization token is missing' do
        before { post "/movies/#{movie_id}/reviews", params: valid_review_attributes, headers: nil }

        it 'returns an authorization error message' do
          expect(json['message']).to match(/Missing token/)
        end
      end
    end
  end
end
