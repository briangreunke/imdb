require 'rails_helper'

RSpec.describe 'Authentication', type: :request do

  describe 'POST auth/login' do

    # Create a user for use in `headers`
    let!(:user) { create(:user) }
    let(:headers) { valid_headers.except('Authorization') }

    let(:valid_credentials) { build_credentials( email=user.email, password=user.password ) }
    let(:fake_credentials) { build_credentials( email=Faker::Internet.email, password=Faker::Internet.password ) }
    let( :no_email_credentials ) { build_credentials( email=nil, password=user.password ) }
    let( :no_password_credentials ) { build_credentials( email=user.email, password=nil ) }
    let( :wrong_password_credentials ) { build_credentials( email=user.email, password=Faker::Internet.password ) }

    context 'when the request is valid' do
      before {post "/auth/login", params: valid_credentials, headers: headers }

      it 'returns an auth token' do
        expect(json['auth_token']).not_to be_nil
      end
    end

    context 'when the request is invalid' do
      context 'when the credentials are invalid' do
        before { post "/auth/login", params: fake_credentials, headers: headers }

        it 'returns an authentication error message' do
          expect(json['message']).to match(/Invalid credentials/)
        end
      end

      context 'when the email is missing' do
        before { post "/auth/login", params: no_email_credentials, headers: headers }

        it 'returns an authentication error message' do
          expect(json['message']).to match(/Invalid credentials/)
        end
      end

      context 'when the password is missing' do
        before { post "/auth/login", params: no_password_credentials, headers: headers }

        it 'returns an authentication error message' do
          expect(json['message']).to match(/Invalid credentials/)
        end
      end

      context 'when the pasword is wrong for the email' do
        before { post "/auth/login", params: wrong_password_credentials, headers: headers }

        it 'returns an authentication error message' do
          expect(json['message']).to match(/Invalid credentials/)
        end
      end

    end
  end
end
