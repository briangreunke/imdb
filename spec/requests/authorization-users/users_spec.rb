require 'rails_helper'

RSpec.describe 'Users API', type: :request do
  let(:user) { create(:user) }
  let(:headers) { valid_headers.except('Authorization') }

  let(:valid_attributes) do
    {
      email: user.email,
      password: user.password,
      password_confirmation: user.password
    }.to_json
  end

  describe 'POST /signup' do
    context 'when the request is valid' do
      before { post '/signup', params: valid_attributes, headers: headers }

      it 'creates a new user' do
        expect(response).to have_http_status(201)
      end

      it 'returns the successful message' do
        expect(json['message']).to match(/Account created/)
      end

      it 'returns an authentication token' do
        expect(json['auth_token']).not_to be_nil
      end
    end

    context 'when the request is invalid' do
      before { post '/signup', params: {}, headers: headers }

      it 'does not create a user' do
        expect(response).to have_http_status(422)
      end

      it 'returns a failure message' do
        expect(json['message'])
          .to match(/Validation failed/)
      end

      # TODO: add more cases here to test for
      # - unmatched passwords
      # - missing email
      # - missing password
    end
  end
end
