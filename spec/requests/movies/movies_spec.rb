require 'rails_helper'

RSpec.describe 'Movies API', type: :request do
 
  let!( :movies ) { create_list( :movie, 10 ) }
  let( :movie_id ) { movies.first.id }

  describe 'GET /movies' do
    before { get '/movies' }

    it 'returns movies' do
      expect(json).not_to be_empty
      expect(json.size).to eq(10)
    end

    it 'returns status code 200' do
      expect(response).to have_http_status(200)
    end
  end

  describe 'GET /movies/:id' do
    before { get "/movies/#{movie_id}" }

    context 'when the movie/id exists' do
      it 'returns movie' do
        expect(json).not_to be_empty
        expect(json['id']).to eq(movie_id)
      end
  
      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end
    end

    context 'when the movie/id does not exist' do
      let(:movie_id) { -1 }
      
      it 'return status code 404' do
        expect(response).to have_http_status(404)
      end

      it 'returns a \'Not found\' message' do
        expect(response.body).to match(/Couldn't find Movie/)
      end
    end
  end

end
