require 'rails_helper'

RSpec.describe 'Movies API', type: :request do
 
  NUMBER_OF_ACTORS = 10
  
  # Create a list of 10 actors to be cast in a movie
  let!( :actors ) { create_list( :actor, NUMBER_OF_ACTORS ) }

  # Assign the actors to the movie
  let(:movie) { create( :movie, actors: actors ) }
  let(:movie_id) { movie.id }

  describe 'GET /movies/:id/actors' do

    before { get "/movies/#{movie_id}/actors" }

    context 'when the movie/id exists' do
      it 'returns movie' do
        expect( json ).not_to be_empty
        expect( json.size ).to eq( NUMBER_OF_ACTORS )
      end
  
      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end
    end

    context 'when the movie/id does not exist' do
      let(:movie_id) { -1 }
      
      it 'return status code 404' do
        expect(response).to have_http_status(404)
      end

      it 'returns a \'Not found\' message' do
        expect(response.body).to match(/Couldn't find Movie/)
      end
    end
  end
end
