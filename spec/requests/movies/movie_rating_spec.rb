require 'rails_helper'

RSpec.describe 'Movies API', type: :request do
 
  let!( :movie1 ) { create( :movie, rating: 4 ) }
  let!( :movie2 ) { create( :movie, rating: 1 ) }
  let!( :movie3 ) { create( :movie, rating: 3 ) }


  describe 'GET /movies/:rating' do

    before { get "/movies/rating/1" }

    it 'returns status code 200' do
      expect(response).to have_http_status(200)
    end

    it 'returns only two movies' do
      expect(json.size).to eq(2)
    end
  end
end
