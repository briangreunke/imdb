require 'rails_helper'

RSpec.describe 'Actors API', type: :request do

  NUMBER_OF_MOVIES = 10

  # Create a list of movies, and then add them to
  # the actor
  let!( :movies ) { create_list( :movie, NUMBER_OF_MOVIES ) }
  let!( :actor ) { create( :actor, movies: movies ) }
  
  let!( :actor_id ) { actor.id }

  describe 'GET /actors/:id/movies' do
    before { get "/actors/#{actor_id}/movies" }

    context 'when the actor/id exists' do
      it 'returns movies' do
        expect(json).not_to be_empty
        expect(json.size).to eq( NUMBER_OF_MOVIES )
       
      end
  
      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end
    end

    context 'when the actor/id does not exist' do
      let(:actor_id) { -1 }
      
      it 'return status code 404' do
        expect(response).to have_http_status(404)
      end

      it 'returns a \'Not found\' message' do
        expect(response.body).to match(/Couldn't find Actor/)
      end
    end
  end

end
