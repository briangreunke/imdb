require 'rails_helper'

RSpec.describe 'Actors API', type: :request do

  NUMBER_OF_ACTORS = 10
  
  let!( :actors ) { create_list( :actor, NUMBER_OF_ACTORS ) }
  let!( :actor_id ) { actors.first.id }

  describe 'GET /actors' do
    before { get '/actors' }

    context 'when there are actors' do
      it 'returns actors' do
        expect(json).not_to be_empty
        expect(json.size).to eq( NUMBER_OF_ACTORS )
      end
  
      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end
    end
  end

  describe 'GET /actors/:id' do
    before { get "/actors/#{actor_id}" }

    context 'when the actor/id exists' do
      it 'returns actor' do
        expect(json).not_to be_empty
        expect(json['id']).to eq(actor_id)
      end
  
      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end
    end

    context 'when the actor/id does not exist' do
      let(:actor_id) { NUMBER_OF_ACTORS + 1 }

      it 'return status code 404' do
        expect(response).to have_http_status(404)
      end

      it 'returns a \'Not found\' message' do
        expect(response.body).to match(/Couldn't find Actor/)
      end
    end
  end

end
